# READ ME

This directory contains the source code for [Google APIs Client Library Plugin
for Hudson][] (the location is subject to change).
It is intended to provide the functions of the [Google APIs Client Library for
Java][] to other plugins.

[Google APIs Client Library Plugin for Hudson]: <https://bitbucket.org/kazssym/hudson-google-api-client-plugin>
[Google APIs Client Library for Java]: <https://code.google.com/p/google-api-java-client/>

## License

This program is licensed under the Apache License, Version 2.0, as the Google
APIs Client Library for Java is.
